# Signed SSH Keys

This repository contains everything you need to sign your own ssh keys and manage them in Git.

## Setup

1. Fork this repository
1. Replace the master key in `keys` by running `ssh-keygen -f ca_key`. 
Make sure you use a strong passphrase (i.e. generate one randomly) and save it in a secure location.
1. Commit the master key.

## Signing a key

1. Get the public keys you want to sign.
1. Sign each public key by running `sign_user_key.sh <path/to/key.pub> <sensible_key_id>`
1. Commit new certificates for future reference (they're saved in `certs`)
1. Distribute certificates to users

Choosing sensible key IDs is important. They will be logged by sshd when the user logs in.
They should be something useful like "Peter's Deployment Key".

The signing script uses the current unix timestamp as the cert's serial number.
The expiration defaults to 5 weeks and can be set in the `variables` file.

## Accepting certificates on a server

To set up a server, do the following:

1. copy the public signing key (`keys/ca_key.pub`) to the host, e.g. to`/etc/ssh`.
2. edit `/etc/ssh/sshd_config` to add `TrustedUserCAKeys /etc/ssh/ca_key.pub`
3. restart sshd

## Troubleshooting

You can inspect ssh certificates by running `ssh-keygen -L -f <filename>` on them.
